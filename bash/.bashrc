############################
#  Useful utility programs #
# ##########################
#
# ncdu is a terminal based disk usage analyzer
#
#
#################################
# setting environment variables #
#################################
export EDITOR=/usr/bin/nvim
export BROWSER=/usr/bin/firefox

if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

#########################
# User Defined alias(s) #
#########################

### utilities
alias r="ranger"
alias nb="newsboat"
alias ping="ping -c 5"
alias ipe="echo -e '\n$(curl -s ipinfo.io/ip)\n'"
alias untar="tar -zxvf"
alias mkdir="mkdir -pv"
alias df="df -h"
alias clearf="clear;neofetch"
alias cat="bat"


alias mv="mv -i"
alias cp="cp -i"
alias rm="rm -i"

alias ..="cd .."
alias ...="cd ../.."

alias ls="ls -lh --color=auto"
alias ls.="ls -lah --color=auto"

alias grep="grep --color=auto"

### neovim (text editor) | https://wiki.gentoo.org/wiki/Neovim
alias vi="nvim"
alias vim="nvim"

### mpv (media player) | https://wiki.gentoo.org/wiki/Mpv
alias mpvplay="mpv --vo=null --video=no --no-video --term-osd-bar --no-resume-playback ./"
alias mpvshuf="mpv --vo=null --video=no --no-video --term-osd-bar --no-resume-playback --shuffle ./"
alias mpvsub="mpv --sub-auto=all"

#########
# Notes #
#########

# Mount NTFS file systems Natively with - mount -t ntfs3 /dev/device /path/to/mountpoint
